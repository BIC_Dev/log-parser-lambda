package guildconfig

import (
	"context"
	"fmt"
	"time"

	"github.com/go-openapi/runtime"
	httptransport "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
)

// GuildConfigService struct
type GuildConfigService struct {
	Client *gcsc.GuildConfigServiceClient
	Auth   runtime.ClientAuthInfoWriter
}

func NewGuildConfigClient(host string, base string, token string) *GuildConfigService {
	transport := httptransport.New(host, base, gcsc.DefaultSchemes)
	client := gcsc.New(transport, strfmt.Default)
	apiKeyAuth := httptransport.APIKeyAuth("Service-Token", "header", token)

	return &GuildConfigService{
		Client: client,
		Auth:   apiKeyAuth,
	}
}

// GetGuildFeed func
func GetGuildFeed(gcs *GuildConfigService, guildID string) (*guild_feeds.GetGuildFeedByIDOK, error) {
	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30 * time.Second)
	guildFeedParams.SetGuild(guildID)
	guildFeedParams.SetGuildID(guildID)
	guildFeedParams.SetContext(context.Background())

	guildFeed, gfErr := gcs.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, gcs.Auth)
	if gfErr != nil {
		if _, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); ok {
			return guildFeed, fmt.Errorf("%s : %s", "Discord server not set up", gfErr)
		}

		return guildFeed, fmt.Errorf("%s : %s", "Failed to get guild feed", gfErr)
	}

	return guildFeed, nil
}

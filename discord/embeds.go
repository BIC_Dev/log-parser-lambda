package discord

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
)

func GenerateEmbeds(fields []*discordgo.MessageEmbedField, logType string, serverName string, serverID string) []*discordgo.MessageEmbed {
	var embeds []*discordgo.MessageEmbed
	embedGenerationTime := time.Now().Unix()

	embedCharCount := 0
	embedFieldCount := 0

	var currentEmbed *discordgo.MessageEmbed = &discordgo.MessageEmbed{
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("%s: %s", logType, serverName),
		Description: fmt.Sprintf("Logs are retrieved every 60 minutes.\nServer ID: %s\nRetrieved: <t:%d:F>.", serverID, embedGenerationTime),
	}

	for _, field := range fields {
		fieldCharCount := len(field.Name) + len(field.Value)

		if embedCharCount+fieldCharCount >= MAX_EMBED_CHARACTERS || embedFieldCount >= MAX_FIELDS {
			embed := *currentEmbed
			currentEmbed = &discordgo.MessageEmbed{
				Type:        discordgo.EmbedTypeRich,
				Title:       fmt.Sprintf("%s: %s", logType, serverName),
				Description: fmt.Sprintf("Logs are retrieved every 60 minutes.\nServer ID: %s\nRetrieved: <t:%d:F>.", serverID, embedGenerationTime),
			}

			embedFieldCount = 0
			embedCharCount = 0

			embeds = append(embeds, &embed)
			continue
		}

		copyField := *field
		embedCharCount += fieldCharCount
		embedFieldCount++

		currentEmbed.Fields = append(currentEmbed.Fields, &copyField)
	}

	if currentEmbed != nil && len(currentEmbed.Fields) != 0 {
		embeds = append(embeds, currentEmbed)
	}

	return embeds
}

package discord

import "github.com/bwmarrin/discordgo"

var MAX_FIELD_CHARACTERS = 900  // 1024
var MAX_FIELDS = 24             // 25
var MAX_EMBED_CHARACTERS = 5300 // 6000

func NewClient(token string) (*discordgo.Session, error) {
	return discordgo.New("Bot " + token)
}

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/bwmarrin/discordgo"
	"github.com/caarlos0/env"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/admin"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/chat"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pve"
	"gitlab.com/BIC_Dev/ark-log-parser/parsermodels/pvp"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/log-parser-lambda/amazon"
	"gitlab.com/BIC_Dev/log-parser-lambda/discord"
	"gitlab.com/BIC_Dev/log-parser-lambda/services/guildconfig"
)

type EnvVars struct {
	Environment             string `env:"ENVIRONMENT,required"`
	AWSRegion               string `env:"AWS_REGION,required"`
	AWSEndpoint             string `env:"AWS_ENDPOINT,required"`
	AWSS3LogLevel           uint   `env:"AWS_S3_LOG_LEVEL,required"`
	DiscordToken            string `env:"DISCORD_TOKEN,required"`
	GuildConfigServiceToken string `env:"GUILD_CONFIG_SERVICE_TOKEN,required"`
	GuildConfigServiceHost  string `env:"GUILD_CONFIG_SERVICE_HOST,required"`
	GuildConfigServiceBase  string `env:"GUILD_CONFIG_SERVICE_HOST,required"`
}

type Handler struct {
	S3          *s3.S3
	Discord     *discordgo.Session
	GuildConfig *guildconfig.GuildConfigService
}

type S3LogDetails struct {
	GuildID    string `json:"guild_id"`
	ServerID   string `json:"server_id"`
	ServerName string `json:"server_name"`
	LogType    string `json:"log_type"`
}

type S3AdminLogs struct {
	Details S3LogDetails     `json:"details"`
	Logs    []admin.AdminLog `json:"logs"`
}

type S3ChatLogs struct {
	Details S3LogDetails   `json:"details"`
	Logs    []chat.ChatLog `json:"logs"`
}

type S3PVPLogs struct {
	Details S3LogDetails `json:"details"`
	Logs    []pvp.PVPLog `json:"logs"`
}

type S3PVELogs struct {
	Details S3LogDetails `json:"details"`
	Logs    []pve.PVELog `json:"logs"`
}

func (h *Handler) handler(ctx context.Context, s3Event events.S3Event) error {
	for _, record := range s3Event.Records {
		s3 := record.S3
		s3Bucket := s3.Bucket.Name

		/*
			S3 Object Key Format
			/{gameType}/{guildID}/{serverID}/{logType}/{timestamp}.json
		*/
		s3Key := s3.Object.Key
		// fmt.Printf("[%s - %s] Bucket = %s, Key = %s \n", record.EventSource, record.EventTime, s3.Bucket.Name, s3.Object.Key)

		s3KeySlice := strings.Split(s3Key, "/")
		if len(s3KeySlice) != 6 {
			return fmt.Errorf("invalid s3 key: %s", s3Key)
		}

		guildID := s3KeySlice[2]
		nitradoID := s3KeySlice[3]
		logType := s3KeySlice[4]
		timestampString := strings.Split(s3KeySlice[5], ".")[0]
		timestamp, piErr := strconv.ParseInt(timestampString, 10, 64)
		if piErr != nil {
			return piErr
		}

		switch logType {
		case "admin":
		case "chat":
		case "pvp":
		case "pve":
		case "tribe":
			return fmt.Errorf("skipping tribe log: %s", s3Key)
		default:
			return fmt.Errorf("unknown log type: %s", s3Key)
		}

		feed, fErr := guildconfig.GetGuildFeed(h.GuildConfig, guildID)
		if fErr != nil {
			return fmt.Errorf("failed to get guild feed : %s", fErr)
		}

		var outputChannel *gcscmodels.ServerOutputChannel
		for _, server := range feed.Payload.Guild.Servers {
			if fmt.Sprint(server.NitradoID) == nitradoID {
				return fmt.Errorf("server not enabled: %s", nitradoID)
			}

			for _, oc := range server.ServerOutputChannels {
				if oc.OutputChannelTypeID == logType {
					outputChannel = oc
					break
				}
			}

			if outputChannel != nil {
				break
			}
		}

		data, dErr := amazon.GetS3Object(h.S3, s3Bucket, s3Key)
		if dErr != nil {
			return dErr
		}

		var embeds []*discordgo.MessageEmbed
		switch logType {
		case "admin":
			logs := &S3AdminLogs{}
			jErr := json.Unmarshal(data.Bytes(), &logs)
			if jErr != nil {
				return fmt.Errorf("failed to unmarshal admin logs: %s", jErr.Error())
			}

			embeds = h.generateAdminLogEmbeds(logs, timestamp)
		case "chat":
			logs := &S3ChatLogs{}
			jErr := json.Unmarshal(data.Bytes(), &logs)
			if jErr != nil {
				return fmt.Errorf("failed to unmarshal chat logs: %s", jErr.Error())
			}

			embeds = h.generateChatLogEmbeds(logs, timestamp)
		case "pvp":
			logs := &S3PVPLogs{}
			jErr := json.Unmarshal(data.Bytes(), &logs)
			if jErr != nil {
				return fmt.Errorf("failed to unmarshal pvp logs: %s", jErr.Error())
			}

			embeds = h.generatePVPLogEmbeds(logs, timestamp)
		case "pve":
			logs := &S3PVELogs{}
			jErr := json.Unmarshal(data.Bytes(), &logs)
			if jErr != nil {
				return fmt.Errorf("failed to unmarshal pve logs: %s", jErr.Error())
			}

			embeds = h.generatePVELogEmbeds(logs, timestamp)
		}

		for i := 0; i < len(embeds); i++ {
			_, smeErr := discord.SendMessageEmbed(h.Discord, outputChannel.ChannelID, embeds[i:i+1])
			if smeErr != nil {
				return fmt.Errorf("failed to send discord message: %d : %s : %s", smeErr.Code, smeErr.Message, smeErr.Err)
			}
		}
	}

	return nil
}

func (h *Handler) generateAdminLogEmbeds(logs *S3AdminLogs, timestamp int64) []*discordgo.MessageEmbed {
	var fields []*discordgo.MessageEmbedField

	var currentUser string
	var tempField *discordgo.MessageEmbedField = &discordgo.MessageEmbedField{
		Name: "\u200b",
	}
	for _, entry := range logs.Logs {
		var entryString string

		switch entry.LogType {
		case "admin":
			entryString = fmt.Sprintf("<t:%d:t> %s", entry.Time, entry.Command)
		default:
			continue
		}

		if len(tempField.Value)+len(tempField.Name)+len(entryString) >= discord.MAX_FIELD_CHARACTERS {
			fields = append(fields, tempField)
			tempField = &discordgo.MessageEmbedField{
				Name: "\u200b",
			}
		}

		if currentUser != entry.PlayerName {
			if tempField.Value == "" {
				tempField.Value += fmt.Sprintf("**%s**\n", entry.PlayerName)
			} else {
				tempField.Value += fmt.Sprintf("\n**%s**\n", entry.PlayerName)
			}
		}

		tempField.Value += fmt.Sprintf("%s\n", entryString)
		currentUser = entry.PlayerName
	}

	if tempField.Value != "" {
		fields = append(fields, tempField)
	}

	return discord.GenerateEmbeds(fields, logs.Details.LogType, logs.Details.ServerName, logs.Details.ServerID)
}

func (h *Handler) generateChatLogEmbeds(logs *S3ChatLogs, timestamp int64) []*discordgo.MessageEmbed {
	var fields []*discordgo.MessageEmbedField

	var currentUser string
	var tempField *discordgo.MessageEmbedField = &discordgo.MessageEmbedField{
		Name: "\u200b",
	}
	for _, entry := range logs.Logs {
		var entryString string

		switch entry.LogType {
		case "chat":
			entryString = fmt.Sprintf("<t:%d:t> %s", entry.Time, entry.Message)
		default:
			continue
		}

		if len(tempField.Value)+len(tempField.Name)+len(entryString) >= discord.MAX_FIELD_CHARACTERS {
			fields = append(fields, tempField)
			tempField = &discordgo.MessageEmbedField{
				Name: "\u200b",
			}
		}

		if currentUser != entry.UserName {
			if tempField.Value == "" {
				tempField.Value += fmt.Sprintf("**%s**\n", entry.UserName)
			} else {
				tempField.Value += fmt.Sprintf("\n**%s**\n", entry.UserName)
			}
		}

		tempField.Value += fmt.Sprintf("%s\n", entryString)
		currentUser = entry.UserName
	}

	if tempField.Value != "" {
		fields = append(fields, tempField)
	}

	return discord.GenerateEmbeds(fields, logs.Details.LogType, logs.Details.ServerName, logs.Details.ServerID)
}

func (h *Handler) generatePVPLogEmbeds(logs *S3PVPLogs, timestamp int64) []*discordgo.MessageEmbed {
	var fields []*discordgo.MessageEmbedField

	var tempField *discordgo.MessageEmbedField = &discordgo.MessageEmbedField{
		Name: "\u200b",
	}
	for _, entry := range logs.Logs {
		var entryString string

		switch entry.LogType {
		case "pvp_dino_killed_player":
			// {killed_player_name} ({killed_player_tribe} lvl {killed_player_level} killed by {killer_name} ({killer_dino_type}) ({killer_tribe}) lvl ({killer_level}))
			entryString = fmt.Sprintf("<t:%d:t> %s (%s) lvl %s killed by %s (%s) (%s) lvl %s", entry.Time, entry.KilledPlayerName, entry.KilledPlayerTribe, entry.KilledPlayerLevel, entry.KillerName, entry.KillerDinoType, entry.KillerTribe, entry.KillerLevel)
		case "pvp_player_killed_player":
			// {killed_player_name} ({killed_player_tribe} lvl {killed_player_level} killed by {killer_name} ({killer_tribe}) lvl ({killer_level}))
			entryString = fmt.Sprintf("<t:%d:t> %s (%s) lvl %s killed by %s (%s) lvl %s", entry.Time, entry.KilledPlayerName, entry.KilledPlayerTribe, entry.KilledPlayerLevel, entry.KillerName, entry.KillerTribe, entry.KillerLevel)
		default:
			continue
		}

		if len(tempField.Value)+len(tempField.Name)+len(entryString) >= discord.MAX_FIELD_CHARACTERS {
			fields = append(fields, tempField)
			tempField = &discordgo.MessageEmbedField{
				Name: "\u200b",
			}
		}

		tempField.Value += fmt.Sprintf("%s\n", entryString)
	}

	if tempField.Value != "" {
		fields = append(fields, tempField)
	}

	return discord.GenerateEmbeds(fields, logs.Details.LogType, logs.Details.ServerName, logs.Details.ServerID)
}

func (h *Handler) generatePVELogEmbeds(logs *S3PVELogs, timestamp int64) []*discordgo.MessageEmbed {
	var fields []*discordgo.MessageEmbedField

	var tempField *discordgo.MessageEmbedField = &discordgo.MessageEmbedField{
		Name: "\u200b",
	}
	for _, entry := range logs.Logs {
		var entryString string

		switch entry.LogType {
		case "pve_dino_killed_player":
			// {killed_player_name} ({killed_player_tribe} lvl {killed_player_level} killed by {killer_name} lvl ({killer_level}))
			entryString = fmt.Sprintf("<t:%d:t> %s (%s) lvl %s killed by %s lvl %s", entry.Time, entry.KilledPlayerName, entry.KilledPlayerTribe, entry.KilledPlayerLevel, entry.KillerName, entry.KillerLevel)
		case "pve_player_die":
			// {killed_player_name} ({killed_player_tribe} lvl {killed_player_level} died
			entryString = fmt.Sprintf("<t:%d:t> %s (%s) lvl %s died", entry.Time, entry.KilledPlayerName, entry.KilledPlayerTribe, entry.KilledPlayerLevel)
		default:
			continue
		}

		if len(tempField.Value)+len(tempField.Name)+len(entryString) >= discord.MAX_FIELD_CHARACTERS {
			fields = append(fields, tempField)
			tempField = &discordgo.MessageEmbedField{
				Name: "\u200b",
			}
		}

		tempField.Value += fmt.Sprintf("%s\n", entryString)
	}

	if tempField.Value != "" {
		fields = append(fields, tempField)
	}

	return discord.GenerateEmbeds(fields, logs.Details.LogType, logs.Details.ServerName, logs.Details.ServerID)
}

func main() {
	envVars := EnvVars{}
	if err := env.Parse(&envVars); err != nil {
		log.Fatalf("environment variable parse failed: %s", err)
	}

	local := envVars.Environment == "local"

	discordSession, dsErr := discord.NewClient(envVars.DiscordToken)
	if dsErr != nil {
		log.Fatalf("failed to open discord session: %s", dsErr.Error())
	}

	sess := amazon.NewSession(envVars.AWSRegion, envVars.AWSEndpoint, envVars.AWSS3LogLevel, local)
	s3Client := amazon.NewS3Client(sess)
	gc := guildconfig.NewGuildConfigClient(envVars.GuildConfigServiceHost, envVars.GuildConfigServiceBase, envVars.GuildConfigServiceToken)

	aHandler := Handler{
		S3:          s3Client,
		Discord:     discordSession,
		GuildConfig: gc,
	}

	// Make the handler available for Remote Procedure Call by AWS Lambda
	lambda.Start(aHandler.handler)
}

package amazon

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
)

func NewSession(region string, endpoint string, logLevel uint, local bool) *session.Session {
	conf := &aws.Config{
		Region:   aws.String(region),
		Endpoint: aws.String(endpoint),
		LogLevel: (*aws.LogLevelType)(aws.Uint(logLevel)),
	}

	if local {
		conf.CredentialsChainVerboseErrors = aws.Bool(true)
		conf.S3ForcePathStyle = aws.Bool(true)
	}

	return session.Must(session.NewSession(conf))
}

func NewS3Client(sess *session.Session) *s3.S3 {
	return s3.New(sess)
}

func NewSQSClient(sess *session.Session) *sqs.SQS {
	return sqs.New(sess)
}

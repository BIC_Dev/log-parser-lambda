package amazon

import (
	"io"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func UploadJSONToS3(client *s3.S3, jsonData io.Reader, bucket string, key string) error {
	uploader := s3manager.NewUploaderWithClient(client)
	_, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   jsonData,
	})

	if err != nil {
		return err
	}

	return nil
}

func GetS3Object(client *s3.S3, bucket string, key string) (*aws.WriteAtBuffer, error) {
	buff := &aws.WriteAtBuffer{}
	download := s3manager.NewDownloaderWithClient(client)

	_, err := download.Download(buff, &s3.GetObjectInput{
		Bucket:              aws.String(bucket),
		Key:                 aws.String(key),
		ResponseContentType: aws.String("application/json"),
	})

	if err != nil {
		return nil, err
	}

	return buff, nil
}
